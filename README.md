# colorpal
**WORK IN PROGRESS**  
At the moment, this is `dominant_color` plus `clap`.

```mono
colorpal 0.0.#-wip
Chip Altman
Your little cli color buddy.

USAGE:
    colorpal [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -i, --image <image>    Specify an <image> file
```

+ [ ] Have some handy default behavior 🤔
+ [x] Accept \<image> argument
+ [x] RGB to stdout (see `dominant_color`)
+ [ ] Transform cleverly
+ [ ] Create Alacritty colorscheme
+ [ ] Get it into alacritty.yaml
