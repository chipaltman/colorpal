#![allow(unused)]

use clap::App;
use clap::Arg;
use cmd_lib;
use regex::bytes::Regex;
use regex::Split;
use std::path;

fn main() {
    let matches = App::new("colorpal")
        .version("0.0.1")
        .about("Your little cli color buddy.")
        .author("Chip Altman")
        .arg(
            Arg::with_name("image")
                .short("i")
                .long("image")
                .takes_value(true)
                .help("Specify an <image> file"),
        )
        .get_matches();

    let image = image::open(&path::Path::new(
        matches.value_of("image").expect("an image file"),
    ))
    .unwrap();

    let _has_alpha = match image.color() {
        image::ColorType::Rgba8 => true,
        image::ColorType::Bgra8 => true,
        _ => false,
    };

    let colors = dominant_color::get_colors(&image.to_bytes(), _has_alpha);

    println!("{} {:?}", _has_alpha, colors);
    println!("     {:?}", colors);
}

/*
 *  true:[252, 252, 252, 255, 88, 99, 114, 255, 251, 177, 52, 255, 109, 121, 131, 255]
 *
 *  Struct regex::Split
 *  ===================
 *
 *  pub struct Split<'r, 't> {
 *      finder: Matches<'r, 't>,
 *      last: usize,
 *  }
 *
 *  Yields all substrings delimited by a regular expression match.
 *  'r is the lifetime of the compiled regular expression
 *      and 't is the lifetime of teh string being split.
 *
 *  Trait Implementations
 *  ---------------------
 *
 *  impl<'r, 't> Iterator for Split<'r, 't> {
 *      type Item = &'t str;
 *
 *      fn next(&mut self) -> Option<&'t str> {
 *          let text = self.finder.0.text();
 *          match self.finder.next() {
 *              None => {
 *                  if self.last > text.len() {
 *                      None
 *                  } else {
 *                      let s = &text[self.last..];
 *                      self.last = text.len() + 1; // Next call will return None
 *                      Some(s)
 *                  }
 *              }
 *              Some(m) => {
 *                  let matched = &text[self.last..m.start()];
 *                  self.last = m.end();
 *                  Some(matched)
 *              }
 *          }
 *      }
 *  }
 *
 *
 *  Struct regex::bytes::Regex
 *  ==========================
 *
 *  #[derive(Clone)]
 *  pub struct Regex(Exec);
 *
 *  impl fmt::Display for Regex {
 *      /// Shows the original regular expression.
 *      fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
 *          write!(f, "{}", self.as_str())
 *      }
 *  }
 *
 *  impl fmt::Debug for Regex {
 *      /// Shows the original regular expression.
 *      fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
 *          fmt::Display::fmt(self, f)
 *      }
 *  }
 */
